class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :message_to_user
  include SessionsHelper









  private

  #Before filters
  def message_to_user
    if user_logged_in? and !current_user.activated?
      message  = "Account not activated. Please check your email "
      message += "#{current_user.email} for the activation link."
      flash.now[:warning] = message
    end
  end
end


