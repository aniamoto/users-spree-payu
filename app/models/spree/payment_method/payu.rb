module Spree
  class PaymentMethod::Payu < PaymentMethod
    def payment_profiles_supported?
      false
    end

    def cancel(*)
    end

    def source_required?
      false
    end

    def credit(*)
      self
    end

    def success?
      true
    end

    def authorization
      self
    end
    def auto_capture?
    true
    end
  end
end

